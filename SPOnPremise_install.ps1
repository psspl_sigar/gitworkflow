$bambooKpiColumn = "Bamboo-KPI-Column"
$pkgName = "bamboo-kpi-column-configuration.sppkg"
$pkgTitle = "Bamboo KPI Column"
$webPartTitle = "KPI Column Configuration"
$path = $(Get-Location).Path


if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{
    Install-Module -Name Microsoft.SharePoint.PowerShell
    Add-PSSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue
   
}

#Add-PSSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue
 
#Set Site variable
$SiteURL= Read-Host -Prompt "Enter Site Collection URL:";


Write-Host Get-SPSite $SiteURL 
#set-sposite $SiteURL -denyaddandcustomizepages $false
# Create Custom Action
Write-Host "Creating custom action...";
$customAction = Get-PnPCustomAction | ? { $_.Name -eq $bambooKpiColumn }
if($customAction -eq $null)
{
	Add-PnPCustomAction -Name $bambooKpiColumn -Title 'Bamboo KPI Column' -Description 'Manage KPI Column configurations' -Group 'SiteActions' -Location 'Microsoft.SharePoint.StandardMenu' -Sequence 10001 -Url ((Get-PnPSite).Url + '/SitePages/' + $bambooKpiColumn + '.aspx')
}


Try {
    #Get the Web
    $Web = Get-SPWeb $SiteURL
  
    #Get the Custom Actions Filter by Title
    $CustomActions = $web.UserCustomActions | Where { $_.Title -eq $CustomActionTitle } | Select ID, Title
 
    If($CustomActions -ne $Null)
    {
        #Delete custom action(s)
        $CustomActions | ForEach-Object {
            #Remove the custom action
            $web.UserCustomActions.Item($_.Id).Delete()
            Write-Host -f Green "Custom Action '$($_.Title)' Deleted Successfully!"
        }
    }
    Else
    {
        write-host -f Yellow "Custom Action '$CustomActionTitle' Doesn't Exist!"
    } 
} Catch {
    Write-Host -ForegroundColor Red "Error:" $_.Exception.Message
}


#Read more: https://www.sharepointdiary.com/2017/07/powershell-to-remove-custom-action-in-sharepoint.html#ixzz65dHK3Sv5



# Replace this with the admin portal url
$adminUrl = "<Admin Portal URL>"
# Replace this with the admin user email address
$adminupn="<Admin User EMail>"

$userCredential = Get-Credential -UserName $adminupn -Message "Type the password."
Connect-SPOService -Url $adminUrl -Credential $userCredential
set-sposite $siteUrl -denyaddandcustomizepages $false

# Connect to the site
$siteUrl = Read-Host -Prompt "Enter Site Collection URL"
Connect-PnPOnline -Url $siteUrl -Credentials (Get-Credential)
set-sposite $siteUrl -denyaddandcustomizepages $false


# Create Custom Action
Write-Host "Creating custom action...";

$customAction = Get-PnPCustomAction | ? { $_.Name -eq $bambooKpiColumn }
if($customAction -eq $null)
{
	Add-PnPCustomAction -Name $bambooKpiColumn -Title 'Bamboo KPI Column' -Description 'Manage KPI Column configurations' -Group 'SiteActions' -Location 'Microsoft.SharePoint.StandardMenu' -Sequence 10001 -Url ((Get-PnPSite).Url + '/SitePages/' + $bambooKpiColumn + '.aspx')
}


#$appInAppCatalog = Get-PnPApp -Identity 1013CDFE-6425-4487-9C5D-75732AA52092
# Add .sppkg to appcatalog
Write-Host "Adding '"$pkgTitle"' to the AppCatalog...";
$app = Add-PnPApp -Path ('./' + $pkgName) -Overwrite

Write-Host "Publishing '"$pkgTitle"' to the AppCatalog...";
Publish-PnPApp -Identity $app.Id

Write-Host "Updating '"$pkgTitle"' to the AppCatalog...";
Update-PnPApp -Identity $app.Id

Write-Host -ForegroundColor Green "Installing app..."
Install-PnPApp -Identity $app



$isPageProvisioned = Get-PnPClientSidePage -Identity $bambooKpiColumn
 if($isPageProvisioned  -eq $null){
     # Create page
    Write-Host "Provisioning Page...";
    Add-PnPClientSidePage -Name $bambooKpiColumn
    Set-PnPClientSidePage -Identity "$bambooKpiColumn" -CommentsEnabled:$false
    Write-Host "Provisioning Section in a page...";    Add-PnPClientSidePageSection -Page $bambooKpiColumn -SectionTemplate OneColumn

 
     #Add-PnPClientSideWebPart -Page $bambooKpiColumn -Component "9ea8c57b-a203-4c4f-9daf-4eac72271b30" -Section 1 -Column 1
     Write-Host "Adding KPI Configuration web part in a page...";
     Add-PnPClientSideWebPart -Page $bambooKpiColumn -Component $webPartTitle -Section 1 -Column 1
 }
# Provision JSLink


Write-Host "Installation of KPI Column completed successfully" -ForegroundColor Green

